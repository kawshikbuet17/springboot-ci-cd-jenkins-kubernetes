package com.example.springbootcicddemoproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SpringbootCiCdDemoProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootCiCdDemoProjectApplication.class, args);
	}

	@GetMapping
	public String message() {
		return "Hello World from Spring Boot CI/CD Demo Project. (And this is the 5th change to lookup CI/CD, using GitLab CI/CD)";
	}
}
