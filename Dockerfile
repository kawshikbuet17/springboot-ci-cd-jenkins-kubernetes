FROM openjdk:17
EXPOSE 8081
ADD target/springboot-ci-cd-jenkins-kubernetes.jar springboot-ci-cd-jenkins-kubernetes.jar
ENTRYPOINT ["java", "-jar", "/springboot-ci-cd-jenkins-kubernetes.jar"]